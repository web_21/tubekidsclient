import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss'],
  providers: [DatePipe]
})
export class UserDetailsComponent implements OnInit {
  
  editForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  message = '';
  pass: string;
  user = JSON.parse(sessionStorage.getItem('currentUser'));

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private datePipe: DatePipe
  ) { }

  ngOnInit() {
    this.editForm = this.formBuilder.group({
      email: ['', Validators.required],
      name: ['', Validators.required],
      lastname: ['', Validators.required],
      country: ['', Validators.required],
      birthday: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      password: ['', Validators.required],
    });
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || 'edit';
    this.getUser();
  }

  getUser(){
    this.userService.getUser(this.user.id)
    .subscribe(
      data => {
        this.pass = data.password;
        this.fillForm(data);
      },
      error =>{
        this.error = error;
        this.loading = false;
      }
    )
  }

  delUser(){
    this.userService.delUser(this.user.id)
    .subscribe(
      data => {
        this.userService.logout();
        this.router.navigate(['login']);
      },
      error =>{
        this.error = error;
        this.loading = false;
      }
    )
  }

  get f() { return this.editForm.controls; }

  fillForm(user){
    this.f.email.setValue(user.email);
    this.f.password.setValue('password');
    this.f.name.setValue(user.name);
    this.f.lastname.setValue(user.lastname);
    this.f.country.setValue(user.country);
    this.f.birthday.setValue(user.birthday);
    this.f.phoneNumber.setValue(user.phoneNumber);
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.editForm.invalid) {
      return;
    }

    let date = this.datePipe.transform(this.f.birthday.value, 'MM/dd/yyyy');

    if (this.f.password.value !== 'password') {
      this.pass = this.f.password.value;
    }

    this.userService.update(
      this.f.email.value,
      this.pass,
      this.f.name.value,
      this.f.lastname.value,
      this.f.country.value,
      date,
      this.f.phoneNumber.value, 
      this.user.id)
      .subscribe(
        data => {
          this.message = 'Profile updated!'
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  }

  home() {
    this.router.navigate(['home']);
  }

  refresh(): void {
    window.location.reload();
  }

  logout() {
    this.userService.logout();
    this.router.navigate(['login']);
  }
}

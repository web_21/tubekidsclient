import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers: [DatePipe]
})

export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private datePipe: DatePipe
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ['', Validators.required],
      name: ['', Validators.required],
      lastname: ['', Validators.required],
      country: ['', Validators.required],
      birthday: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      password: ['', Validators.required],
    });
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || 'register';
  }

  loginVisible() {
    this.router.navigate(['login']);
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    let date = this.datePipe.transform(this.f.birthday.value, 'MM/dd/yyyy');
    
    this.userService.register(
      this.f.email.value,
      this.f.password.value,
      this.f.name.value,
      this.f.lastname.value,
      this.f.country.value,
      date,
      this.f.phoneNumber.value)
      .subscribe(
        data => {
          this.router.navigate(['/verify/'+data.id]); //account verification page
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  }
}

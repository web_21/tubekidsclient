import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { VideoService } from '../../services/video.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-video-details',
  templateUrl: './video-details.component.html',
  styleUrls: ['./video-details.component.scss']
})
export class VideoDetailsComponent implements OnInit {

  editForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  message = '';
  msjDel = '';
  videoId: string;
  user = JSON.parse(sessionStorage.getItem('currentUser'));

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private videoService: VideoService,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.editForm = this.formBuilder.group({
      name: ['', Validators.required],
      url: ['', Validators.required]
    });
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || 'video/edit/' + this.videoId;
    this.route.paramMap.subscribe(params => {
      this.videoId = params.get("id");
      this.getVideo();
    });
  }

  getVideo(){
    this.videoService.getVideo(this.videoId)
    .subscribe(
      data => {
        this.fillForm(data);
      },
      error =>{
        this.error = error;
        this.loading = false;
      }
    )
  }

  delVideo(){
    this.videoService.delVideo(this.videoId)
    .subscribe(
      data => {
        this.msjDel = 'Video deleted!'
      },
      error =>{
        this.error = error;
        this.loading = false;
      }
    )
  }

  get f() { return this.editForm.controls; }

  fillForm(user){
    this.f.name.setValue(user.name);
    this.f.url.setValue(user.url);
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.editForm.invalid) {
      return;
    }

    this.videoService.update(
      this.f.name.value,
      this.f.url.value, 
      this.videoId)
      .subscribe(
        data => {
          this.message = 'Video updated!'
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  }

  home() {
    this.router.navigate(['home']);
  }

  refresh(): void {
    window.location.reload();
  }

  logout() {
    this.userService.logout();
    this.router.navigate(['login']);
  }
}
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss']
})
export class VerifyComponent implements OnInit {

  verifyForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  userId: string;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.verifyForm = this.formBuilder.group({
      code: ['', Validators.required]
    });
    this.route.paramMap.subscribe(params => {
      this.userId = params.get('id');
    });
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/verify/'+this.userId;
  }

  loginVisible() {
    this.router.navigate(['login']);
  }

  registerVisible() {
    this.router.navigate(['register']);
  }

  get f() { return this.verifyForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.verifyForm.invalid) {
      return;
    }

    this.loading = true;

    if (this.f.code.value.length !== 6) {
      this.loading = false;
      return this.error = 'Invalid code';
    }

    this.f.code.setValue(this.f.code.value.toUpperCase());

    this.userService.verify(this.f.code.value.toUpperCase(), this.userId)
      .subscribe(
        data => {
          this.router.navigate(['login']);
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  }
}

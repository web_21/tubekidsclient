import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { KidService } from '../../services/kid.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-kid-details',
  templateUrl: './kid-details.component.html',
  styleUrls: ['./kid-details.component.scss']
})
export class KidDetailsComponent implements OnInit {

  editForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  message = '';
  msjDel = '';
  pass: string;
  kidId: string;
  user = JSON.parse(sessionStorage.getItem('currentUser'));

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private kidService: KidService,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.editForm = this.formBuilder.group({
      name: ['', Validators.required],
      username: ['', Validators.required],
      age: ['', Validators.required],
      pin: ['', Validators.required]
    });
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || 'kid/edit/' + this.kidId;
    this.route.paramMap.subscribe(params => {
      this.kidId = params.get("id");
      this.getKid();
    });
  }

  getKid(){
    this.kidService.getKid(this.kidId)
    .subscribe(
      data => {
        this.pass = data.pin;
        this.fillForm(data);
      },
      error =>{
        this.error = error;
        this.loading = false;
      }
    )
  }

  delKid(){
    this.kidService.delKid(this.kidId)
    .subscribe(
      data => {
        this.msjDel = 'User deleted!'
      },
      error =>{
        this.error = error;
        this.loading = false;
      }
    )
  }

  get f() { return this.editForm.controls; }

  fillForm(user){
    this.f.name.setValue(user.fullName);
    this.f.username.setValue(user.userName);
    this.f.pin.setValue('kidPin');
    this.f.age.setValue(user.age);
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.editForm.invalid) {
      return;
    }

    if (this.f.pin.value !== 'kidPin') {
      this.pass = this.f.pin.value;
    }

    if (this.f.pin.value.length !== 6) {
      this.loading = false;
      return this.error = 'Pin must be 6 digits long';
    }

    this.kidService.update(
      this.f.name.value,
      this.f.username.value,
      this.pass,
      this.f.age.value, 
      this.kidId)
      .subscribe(
        data => {
          this.message = 'Profile updated!'
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  }

  home() {
    this.router.navigate(['home']);
  }

  refresh(): void {
    window.location.reload();
  }

  logout() {
    this.userService.logout();
    this.router.navigate(['login']);
  }
}
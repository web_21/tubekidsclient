import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { KidService } from '../../services/kid.service';

@Component({
  selector: 'app-kid-login',
  templateUrl: './kid-login.component.html',
  styleUrls: ['./kid-login.component.scss']
})
export class KidLoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private kidService: KidService
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      pin: ['', Validators.required]
    });
    this.kidService.logout();
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || 'kid/login';
  }

  parentsLogin() {
    this.router.navigate(['login']);
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;

    if (this.f.pin.value.length !== 6) {
      this.loading = false;
      return this.error = 'Pin must be 6 digits long';
    }
    this.kidService.login(this.f.username.value, this.f.pin.value)
      .subscribe(
        data => {
          this.router.navigate(['kid/home']);
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  }
}

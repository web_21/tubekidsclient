import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KidLoginComponent } from './kid-login.component';

describe('KidLoginComponent', () => {
  let component: KidLoginComponent;
  let fixture: ComponentFixture<KidLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KidLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KidLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

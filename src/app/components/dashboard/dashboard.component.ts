import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';
import { KidService } from '../../services/kid.service';
import { VideoService } from '../../services/video.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  returnUrl: string;
  user = JSON.parse(sessionStorage.getItem('currentUser'));
  error = '';
  loading = false;
  videos = [];
  kids = [];
  urlSafe: SafeResourceUrl;
  videoForm: FormGroup;
  kidForm: FormGroup;
  filterForm: FormGroup;
  submitted = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    private sanitizer: DomSanitizer,
    private formBuilder: FormBuilder,
    private kidService: KidService,
    private videoService: VideoService
  ) { }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || 'home';
    this.getVideos();
    this.getKids();
    this.videoForm = this.formBuilder.group({
      name: ['', Validators.required],
      url: ['', Validators.required]
    });
    this.kidForm = this.formBuilder.group({
      fullName: ['', Validators.required],
      username: ['', Validators.required],
      pin: ['', Validators.required],
      age: ['', Validators.required],
    });
    this.filterForm = this.formBuilder.group({
      filter: ['']
    })
  }

  logout() {
    this.userService.logout();
    this.router.navigate(['login']);
  }

  edit() {
    this.router.navigate(['edit']);
  }

  getVideos(){
    this.videoService.getVideos(this.user.id)
    .subscribe(
      data => {
        this.sanitizeUrls(data);
      },
      error =>{
        this.error = error;
        this.loading = false;
      }
    )
  }

  kidDetails(id: string){
    this.router.navigate(['kid/edit/' + id]);
  }

  videoDetails(id: string){
    this.router.navigate(['video/edit/' + id]);
  }

  getKids(){
    this.kidService.getKids(this.user.id)
    .subscribe(
      data => {
       this.kids = data;
      },
      error =>{
        this.error = error;
        this.loading = false;
      }
    )
  }

  sanitizeUrls(list){
    list.forEach(video => {
      this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(video.url);
      video.url = this.urlSafe;
      this.videos.push(video);
    });
  }

  get fv() { return this.videoForm.controls; }
  get fk() { return this.kidForm.controls; }
  get ff() { return this.filterForm.controls; }

  onSubmitV() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.videoForm.invalid) {
      return;
    }

    this.loading = true;

    this.videoService.save(this.fv.name.value, this.fv.url.value, this.user.id)
    .subscribe(
      data => {
        this.refresh();
      }
    );
  }

  search(){
    this.submitted = true;

    // stop here if form is invalid
    if (this.filterForm.invalid) {
      return;
    }

    this.loading = true;

    this.videoService.search(this.ff.filter.value, this.user.id)
    .subscribe(
      data => {
        this.videos.splice(0,this.videos.length);
        this.sanitizeUrls(data);
      }
    )
  }

  onSubmitK(){
    this.submitted = true;

    // stop here if form is invalid
    if (this.kidForm.invalid) {
      return;
    }

    this.loading = true;

    if (this.fk.pin.value.length !== 6) {
      this.loading = false;
      return this.error = 'Pin must be 6 digits long';
    }
    this.kidService.register(this.fk.fullName.value, this.fk.username.value, this.fk.pin.value, this.fk.age.value, this.user.id)
    .subscribe(
      data => {
        this.refresh();
      }
    );
  }

  refresh(): void {
    window.location.reload();
  }
}

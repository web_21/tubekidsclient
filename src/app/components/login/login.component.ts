import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.userService.logout();
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || 'login';
  }

  registerVisible() {
    this.router.navigate(['register']);
  }

  kidsLogin() {
    this.router.navigate(['kid/login']);
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;

    this.userService.login(this.f.email.value, this.f.password.value)
      .subscribe(
        data => {
          this.router.navigate(['/authenticate/'+data.id]); //authentication page
        },
        error => {
          if (error === 'unverified') {
            this.verify(this.f.email.value, this.f.password.value)
          }
          this.error = error;
          this.loading = false;
        });
  }

  verify(email: string, password: string){
    this.userService.verifyAcc(email, password)
    .subscribe(
      data => {
        this.router.navigate(['/verify/' + data.id]); //account verification page
      }
    )
  }
}

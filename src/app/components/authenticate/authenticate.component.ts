import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-authenticate',
  templateUrl: './authenticate.component.html',
  styleUrls: ['./authenticate.component.scss']
})
export class AuthenticateComponent implements OnInit {

  authForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  userId: string;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.authForm = this.formBuilder.group({
      code: ['', Validators.required]
    });
    this.route.paramMap.subscribe(params => {
      this.userId = params.get('id');
    });
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/authenticate/'+this.userId;
  }

  loginVisible() {
    this.router.navigate(['login']);
  }

  registerVisible() {
    this.router.navigate(['register']);
  }

  get f() { return this.authForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.authForm.invalid) {
      return;
    }

    this.loading = true;

    if (this.f.code.value.length !== 6) {
      this.loading = false;
      return this.error = 'Invalid code';
    }

    this.f.code.setValue(this.f.code.value.toUpperCase());

    this.userService.auth(this.f.code.value.toUpperCase(), this.userId)
      .subscribe(
        data => {
          this.router.navigate(['home']);
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  }
}
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { KidService } from '../../services/kid.service';
import { VideoService } from '../../services/video.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-kid-dashboard',
  templateUrl: './kid-dashboard.component.html',
  styleUrls: ['./kid-dashboard.component.scss']
})
export class KidDashboardComponent implements OnInit {

  returnUrl: string;
  user = JSON.parse(sessionStorage.getItem('currentUser'));
  error = '';
  loading = false;
  videos = [];
  urlSafe: SafeResourceUrl;
  filterForm: FormGroup;
  submitted = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private kidService: KidService,
    private sanitizer: DomSanitizer,
    private formBuilder: FormBuilder,
    private videoService: VideoService
  ) { }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || 'kid/home';
    this.getVideos();
    this.filterForm = this.formBuilder.group({
      filter: ['']
    })
  }

  logout() {
    this.kidService.logout();
    this.router.navigate(['kid/login']);
  }

  getVideos(){
    this.videoService.getVideos(this.user.parentId)
    .subscribe(
      data => {
        this.sanitizeUrls(data);
      },
      error =>{
        this.error = error;
        this.loading = false;
      }
    )
  }
  
  sanitizeUrls(list){
    list.forEach(video => {
      this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(video.url);
      video.url = this.urlSafe;
      this.videos.push(video);
    });
  }

  get ff() { return this.filterForm.controls; }

  search(){
    this.submitted = true;

    // stop here if form is invalid
    if (this.filterForm.invalid) {
      return;
    }

    this.loading = true;

    this.videoService.search(this.ff.filter.value, this.user.parentId)
    .subscribe(
      data => {;
        this.videos.splice(0,this.videos.length);
        this.sanitizeUrls(data);
      }
    )
  }

  refresh(): void {
    window.location.reload();
  }
}

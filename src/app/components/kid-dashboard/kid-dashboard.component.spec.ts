import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KidDashboardComponent } from './kid-dashboard.component';

describe('KidDashboardComponent', () => {
  let component: KidDashboardComponent;
  let fixture: ComponentFixture<KidDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KidDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KidDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

export class UserKid {
    public id: string;
    public fullName: string;
    public userName: string;
    //pin must have 6 digits
    public pin: string;
    public age: number;
    public parentId: string;
    public token: string;
}
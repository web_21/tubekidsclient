export class User {
    public id: string;
    public email: string;
    public password: string;
    public name: string;
    public lastname: string;
    public country: string;
    //format mm/dd/yyyy
    public birthday: string;
    public phoneNumber: number;
    public verified: string;
    public verificationCode: string;
    public token: string;
}

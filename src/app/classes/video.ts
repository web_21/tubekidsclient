export class Video {
    public id: string;
    public name: string;
    public url: string;
    public userId: string;
}

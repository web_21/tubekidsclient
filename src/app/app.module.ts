import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faSearch, faPlus, faUser } from '@fortawesome/free-solid-svg-icons';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { JwtInterceptor } from './helpers/jwt.interceptor';
import { ErrorInterceptor } from './helpers/error.interceptor';
import { KidLoginComponent } from './components/kid-login/kid-login.component';
import { RegisterComponent } from './components/register/register.component';
import { VerifyComponent } from './components/verify/verify.component';
import { AuthenticateComponent } from './components/authenticate/authenticate.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { KidDashboardComponent } from './components/kid-dashboard/kid-dashboard.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { KidDetailsComponent } from './components/kid-details/kid-details.component';
import { VideoDetailsComponent } from './components/video-details/video-details.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    KidLoginComponent,
    RegisterComponent,
    VerifyComponent,
    AuthenticateComponent,
    DashboardComponent,
    KidDashboardComponent,
    UserDetailsComponent,
    KidDetailsComponent,
    VideoDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FontAwesomeModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private library: FaIconLibrary){
    library.addIcons(faSearch, faPlus, faUser);
  }
}

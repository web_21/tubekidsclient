import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { LoginComponent } from './components/login/login.component';
import { KidLoginComponent } from './components/kid-login/kid-login.component';
import { RegisterComponent } from './components/register/register.component';
import { VerifyComponent } from './components/verify/verify.component';
import { AuthenticateComponent } from './components/authenticate/authenticate.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { KidDashboardComponent } from './components/kid-dashboard/kid-dashboard.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { KidDetailsComponent } from './components/kid-details/kid-details.component';
import { VideoDetailsComponent } from './components//video-details/video-details.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'kid/login', component: KidLoginComponent },
  { path: 'register' , component: RegisterComponent },
  { path: 'verify/:id' , component: VerifyComponent },
  { path: 'authenticate/:id' , component: AuthenticateComponent },
  { path: 'home' , component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'kid/home' , component: KidDashboardComponent, canActivate: [AuthGuard] },
  { path: 'kid/home' , component: KidDashboardComponent, canActivate: [AuthGuard] },
  { path: 'edit' , component: UserDetailsComponent, canActivate: [AuthGuard] },
  { path: 'kid/edit/:id' , component: KidDetailsComponent, canActivate: [AuthGuard] },
  { path: 'video/edit/:id' , component: VideoDetailsComponent, canActivate: [AuthGuard] },
  { path: '**', redirectTo: 'login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routing = RouterModule.forRoot(routes);
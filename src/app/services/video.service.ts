import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class VideoService {

  private videoUrl: string;

  constructor(private http: HttpClient, private router: Router) {
    this.videoUrl = 'http://localhost:8000/video';
  }

  getVideos(id: string) {
    return this.http.get<any>(this.videoUrl + '/byuser/' + id)
      .pipe(map(res => {
        return res;
      }))
  }

  save(name: string, url: string, userId: string) {
    return this.http.post<any>(this.videoUrl + '/add', { name, url, userId })
      .pipe(map(res => {
        return res;
      }))
  }

  search(filter: string, id: string) {
    return this.http.post<any>(this.videoUrl + '/filter/' + id, { filter })
      .pipe(map(res => {
        return res;
      }))
  }

  delAll(id: string) {
    return this.http.delete<any>(this.videoUrl + "/byUser/" + id)
      .pipe(map(
        res => {
          return res;
        }))
  }

  getVideo(id: string) {
    return this.http.get<any>(this.videoUrl + '/' + id)
      .pipe(map(res => {
        return res;
      }))
  }

  delVideo(id: string) {
    return this.http.delete<any>(this.videoUrl + "/" + id)
      .pipe(map(
        res => {
          return res;
        }))
  }

  update(name: string, url: string, id: string){
    return this.http.post<any>(this.videoUrl + '/edit/' + id, { name, url })
      .pipe(map(res => {
        return res;
      }))
  }
}

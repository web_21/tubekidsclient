import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class KidService implements OnInit {

  private url: string;
  loading = false;
  submitted = false;
  error = '';

  constructor(private http: HttpClient, private router: Router) {
    this.url = 'http://localhost:8000/kid';
  }

  ngOnInit() {
    this.logout();
  }

  login(userName: string, pin: string) {
    //console.log('Username: ', userName, ' Password: ', pin);
    return this.http.post<any>(this.url + '/login', { userName, pin })
      .pipe(map(res => {
        if (res && res.id) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          sessionStorage.setItem('currentUser', JSON.stringify(res));
          localStorage.setItem('user', 'kid');
        };
        return res;
      }));
  };

  logout() {
    // remove user from local storage to log user out
    sessionStorage.removeItem('currentUser');
    localStorage.removeItem('user');
  }

  register(fullName: string, userName: string, pin: string, age: number, parentId: string) {
    return this.http.post<any>(this.url + '/add', { fullName, userName, pin, age, parentId })
      .pipe(map(res => {
        return res;
      }));
  };

  getKids(id: string) {
    return this.http.get<any>(this.url + '/byparent/' + id)
      .pipe(map(res => {
        return res;
      }))
  }

  delAll(id: string) {
    return this.http.delete<any>(this.url + "/byParent/" + id)
      .pipe(map(res => {
        return res;
      }))
  }

  getKid(id: string) {
    return this.http.get<any>(this.url + '/' + id)
      .pipe(map(res => {
        return res;
      }))
  }

  delKid(id: string) {
    return this.http.delete<any>(this.url + "/" + id)
      .pipe(map(res => {
        return res;
      }))
  }

  update(fullName: string, userName: string, pin: string, age: number, id: string){
    return this.http.post<any>(this.url + '/edit/' + id, { fullName, userName, pin, age })
      .pipe(map(res => {
        return res;
      }));
  }
}

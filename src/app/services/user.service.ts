import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { KidService } from './kid.service';
import { VideoService } from './video.service';

@Injectable({
  providedIn: 'root'
})
export class UserService implements OnInit {

  private url: string;

  constructor(
    private http: HttpClient,
    private router: Router,
    private kidService: KidService,
    private videoService: VideoService
  ) {
    this.url = 'http://localhost:8000/user';
  }

  ngOnInit() {
    this.logout();
  }

  login(email: string, password: string) {
    return this.http.post<any>(this.url + '/login', { email, password })
      .pipe(map(res => {
        return res;
      }));
  }

  logout() {
    // remove user from local storage to log user out
    sessionStorage.removeItem('currentUser');
    localStorage.removeItem('user');
  }

  register(email: string, password: string, name: string, lastname: string, country: string, birthday: string, phoneNumber: number) {
    return this.http.post<any>(this.url + '/add', { email, password, name, lastname, country, birthday, phoneNumber })
      .pipe(map(data => {
        return data;
      }));
  }

  verify(code: string, userId: string) {
    return this.http.post<any>(this.url + '/verify/' + userId, { code })
      .pipe(map(res => {
        return res;
      }));
  };

  verifyAcc(email: string, password: string){
    return this.http.post<any>(this.url + '/verifyAcc', { email, password })
      .pipe(map(res => {
        return res;
      }));
  }

  auth(code: string, userId: string) {
    return this.http.post<any>(this.url + '/authenticate/' + userId, { code })
      .pipe(map(res => {
        if (res && res.id) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          sessionStorage.setItem('currentUser', JSON.stringify(res));
          localStorage.setItem('user', 'parent');
        }
        return res;
      }));
  };

  getUser(id: string) {
    return this.http.get<any>(this.url + "/" + id)
      .pipe(map(res => {
        return res;
      }));
  };

  update(email: string, password: string, name: string, lastname: string, country: string, birthday: string, phoneNumber: number, id: string) {
    return this.http.post<any>(this.url + '/edit/' + id, { email, password, name, lastname, country, birthday, phoneNumber })
      .pipe(map(data => {
        return data;
      }));
  }

  delUser(id: string) {
    return this.http.delete<any>(this.url + "/" + id)
      .pipe(map(data => {
        this.kidService.delAll(id).subscribe()
        this.videoService.delAll(id).subscribe()
        return data;
      }))
  }
};